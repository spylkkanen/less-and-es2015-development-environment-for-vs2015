
# Project configuration #

## Gulp script ##
Gulp script compiles ES2015 javascript files and LESS files to delivery packages. 

Web application should not use original source files for application execution. Web application should use only compiles versions of javascript files and css files.
>Do not use original javascript files or CSS files in website.

## Project file configuration ##
We need to help WebDeploy build process to collect required files and folders for deployment package. Also we need help WebDeploy build process to skip unwanted files and folders.
>Simply add only required files and folders to publish pagkage.

e.g. Original javascript files and LESS files are not needed running application. 

Define publish build to exclude unwanted files and folder. Add definitions for Debug and Release coonfiguration.
```
 <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' ">
    ...
    <ExcludeFilesFromDeployment>
      package.json;packages.config;gulpfile.js;.npmrc;
    </ExcludeFilesFromDeployment>
    <ExcludeFoldersFromDeployment>
      js_src;css_src;
    </ExcludeFoldersFromDeployment>
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|AnyCPU' ">
    ...
    <ExcludeFilesFromDeployment>
      package.json;packages.config;gulpfile.js;.npmrc;
    </ExcludeFilesFromDeployment>
    <ExcludeFoldersFromDeployment>
      js_src;css_src;
    </ExcludeFoldersFromDeployment>
  </PropertyGroup>
```

Define publish build scripts to collect required files and folders. Add defintion after import element.
>Simply add definition end of project element.

```
  <PropertyGroup>
    <CompileDependsOn>
      $(CompileDependsOn);
      GulpBuild;
    </CompileDependsOn>
    <CleanDependsOn>
      $(CleanDependsOn);
      GulpClean;
    </CleanDependsOn>
    <CopyAllFilesToSingleFolderForPackageDependsOn>
	  $(CopyAllFilesToSingleFolderForPackageDependsOn);
      CollectGulp_CSS_output;
	  CollectGulp_JS_output;
	</CopyAllFilesToSingleFolderForPackageDependsOn>
    <CopyAllFilesToSingleFolderForMsdeployDependsOn>
      $(CopyAllFilesToSingleFolderForMsdeployDependsOn);
	  CollectGulp_CSS_output;
	  CollectGulp_JS_output;
	  </CopyAllFilesToSingleFolderForMsdeployDependsOn>
  </PropertyGroup>
  <Target Name="GulpBuild">
    <Exec Command="npm install" />
    <Exec Command="gulp --mode $(ConfigurationName)" />
  </Target>
  <Target Name="GulpClean">
    <Exec Command="gulp clean" />
  </Target>
  <Target Name="CollectGulp_JS_output">
    <ItemGroup>
      <_CustomFiles_JS Include=".\js\**\*" />
      <FilesForPackagingFromProject Include="%(_CustomFiles_JS.Identity)">
        <DestinationRelativePath>js\%(RecursiveDir)%(Filename)%(Extension)</DestinationRelativePath>
      </FilesForPackagingFromProject>
    </ItemGroup>
    <Message Text="CollectGulp_JS_output list: %(_CustomFiles_JS.Identity)" />
  </Target>
  <Target Name="CollectGulp_CSS_output">
    <ItemGroup>
      <_CustomFiles_CSS Include=".\css\**\*" />
      <FilesForPackagingFromProject Include="%(_CustomFiles_CSS.Identity)">
        <DestinationRelativePath>css\%(RecursiveDir)%(Filename)%(Extension)</DestinationRelativePath>
      </FilesForPackagingFromProject>
    </ItemGroup>
    <Message Text="CollectGulp_CSS_output list: %(_CustomFiles_CSS.Identity)" />
  </Target>
```

## Publish profile configuration ##
Select project and click mouse right click and define publish profile.

# References #
## Based on ##
http://getbootstrap.com/examples/dashboard/

## Web deploy + gulpfile.js ##
http://www.codecadwallader.com/2015/03/15/integrating-gulp-into-your-tfs-builds-and-web-deploy/
