﻿/*jshint globalstrict:true, devel:true, newcap:false */
/*global require */

/**
 * Build CSS and JavaScript using `gulp`.
 *
 * Main targets are: `js`, `css` and `watch`.
 *
 * Run with `--mode Debug` to get unminified sources and map files.
 * Run with `--mode Release` to get minified sources.
 */

"use strict";

var argv = require('yargs').argv,

    gulp = require('gulp'),
    gutil = require('gulp-util'),
    gulpif = require('gulp-if'),

    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    sourcemaps = require('gulp-sourcemaps'),
    browserify = require('browserify'),
    babelify = require('babelify'),
    watchify = require('watchify'),
    // reactify   = require('reactify'),
    uglify = require('gulp-uglify'),

    concat = require('gulp-concat'),
    rename = require('gulp-rename'),

    clean = require('gulp-clean'),

    less = require('gulp-less'),
    minifyCSS = require('gulp-minify-css'),

    header = require('gulp-header'), // assign the module to a local variable

    gulp = require('gulp'),
    babel = require('gulp-babel'),

    pkg = require('./package.json');

// Is debug or Release
var isDebugBuild = 'Debug' === argv.mode;
gutil.log('Build mode: ' + argv.mode);

// Create file header banner.
var banner = ['/**',
  ' * <%= pkg.name %> - <%= pkg.description %>',
  ' * @version v<%= pkg.version %>',
  ' * @link <%= pkg.homepage %>',
  ' * @license <%= pkg.license %>',
  ' */',
  ''].join('\n');

// Directory where static files are found.
var staticStyleSheetDirectory = './css_src/app/',
    staticJavascriptDirectory = './js_src/app/',
    vendorJavascriptDestinationDirectory = './js/',
    vendorStyleSheetDestinationDirectory = './css/';

// Remove generated css file.
gulp.task('css_clean', function () {
    return gulp.src(vendorStyleSheetDestinationDirectory + 'app.css')
        .pipe(clean({ force: true }));
});

// Build css from less.
gulp.task('css', ['css_clean'], function () {
    return gulp.src(staticStyleSheetDirectory + 'app.less')
        .pipe(less()) // less conversion.
        .pipe(gulpif(!isDebugBuild/*argv.production*/, minifyCSS({ keepBreaks: false }))) // minify stylesheet files.
        .pipe(header(banner, { pkg: pkg })) // add file header banner text.
        .pipe(gulp.dest(vendorStyleSheetDestinationDirectory));
});

// Remove generated css file.
gulp.task('css_vendor_clean', function () {
    return gulp.src(vendorStyleSheetDestinationDirectory + 'vendor.css')
        .pipe(clean({ force: true }));
});

// Build js file (concat and uglify)
gulp.task('css_vendor', ['css_vendor_clean'], function () {
    return gulp.src([
          './node_modules/bootstrap/dist/css/bootstrap.css'
    ])
        .pipe(concat('vendor.css')) // concat all js files to single file.
        .pipe(gulpif(!isDebugBuild/*argv.production*/, minifyCSS({ keepBreaks: false }))) // minify stylesheet files.
        .pipe(gulp.dest(vendorStyleSheetDestinationDirectory));
});

// Remove generated javascript file.
gulp.task('js_clean', function () {
    return gulp.src(vendorJavascriptDestinationDirectory + 'app.js')
        .pipe(clean({ force: true }));
});

// Remove generated javascript source map file.
gulp.task('js_map_clean', function () {
    return gulp.src(vendorJavascriptDestinationDirectory + '*.map')
        .pipe(clean({ force: true }));
});

// Build js file (concat and uglify)
gulp.task('js', ['js_map_clean', 'js_clean'], function () {
    //return gulp.src(staticJavascriptDirectory + '/**/*.js')
    //    .pipe(babel()) // ES2015 conversion.
    //    .pipe(concat('app.js')) // concat all js files to single file.
    //    .pipe(sourcemaps.init({ loadMaps: true }))
    //    .pipe(gulpif(isDebugBuild/*argv.production*/, sourcemaps.write('./'))) // writes .map file // loads map from browserify file
    //    .pipe(gulpif(!isDebugBuild/*argv.production*/, uglify())) // minify javascript files.
    //    .pipe(header(banner, { pkg: pkg })) // add file header banner text.
    //    .pipe(gulp.dest(vendorJavascriptDestinationDirectory));

    // app.js is your main JS file with all your module inclusions
    return browserify({ entries: staticJavascriptDirectory + '/app.js', debug: true })
        .transform("babelify", { presets: ["es2015"] })
        .bundle()
        .pipe(source('app.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init())
        .pipe(gulpif(isDebugBuild/*argv.production*/, sourcemaps.write('./'))) // writes .map file // loads map from browserify file
        .pipe(gulpif(!isDebugBuild/*argv.production*/, uglify())) // minify javascript files.
        .pipe(header(banner, { pkg: pkg })) // add file header banner text.
        .pipe(gulp.dest(vendorJavascriptDestinationDirectory));
    //.pipe(livereload());
});

// Remove generated javascript file.
gulp.task('js_vendor_clean', function () {
    return gulp.src(vendorJavascriptDestinationDirectory + 'vendor.js')
        .pipe(clean({ force: true }));
});

// Build js file (concat and uglify)
gulp.task('js_vendor', ['js_vendor_clean'], function () {
    return gulp.src([
          //'./node_modules/requirejs/require.js',
          //'./node_modules/commonjs/lib/system.js',
          './node_modules/jquery/dist/jquery.js',
          './node_modules/bootstrap/dist/js/bootstrap.js'
    ])
        .pipe(concat('vendor.js')) // concat all js files to single file.
        .pipe(sourcemaps.init())
        .pipe(gulpif(isDebugBuild/*argv.production*/, sourcemaps.write('./'))) // writes .map file // loads map from browserify file
        .pipe(gulpif(!isDebugBuild/*argv.production*/, uglify())) // minify javascript files.
        .pipe(gulp.dest(vendorJavascriptDestinationDirectory));
});

// gulp.task('csswatch', function () {
//     gulp.watch(staticStyleSheetDirectory, ['css']);
// });

// gulp.task('jswatch', function () {
//     gulp.watch(staticJavascriptDirectory, ['less']);
// });

gulp.task('clean', ['css_vendor_clean', 'css_clean', 'js_vendor_clean', 'js_clean', 'js_map_clean']);

// gulp.task('watch', ['jswatch', 'csswatch']);
gulp.task('default', ['js', 'js_vendor', 'css', 'css_vendor']);