/**
 * aspnet - Test applications
 * @version v1.2.3
 * @link http://www.test_dummy_domain.fi
 * @license GPL
 */
(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

var _person = require("./person");

var _person2 = _interopRequireDefault(_person);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var person = new _person2.default("Ram", "Kulkarni");

//document.getElementById("nameSpan").innerHTML = person.getFirstName() + " " + person.getLastName();

$('#mytable tbody tr').click(function (event) {
    //$(this).addClass('highlight').siblings().removeClass('highlight');
    $(this).addClass('active').siblings().removeClass('active');
});

},{"./person":2}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Person = function () {
    function Person(firstName, lastName) {
        _classCallCheck(this, Person);

        this.firstName = firstName;
        this.lastName = lastName;
    }

    _createClass(Person, [{
        key: "getFirstName",
        value: function getFirstName() {
            return this.firstName;
        }
    }, {
        key: "getLastName",
        value: function getLastName() {
            return this.lastName;
        }
    }]);

    return Person;
}();

exports.default = Person;

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJqc19zcmNcXGFwcFxcYXBwLmpzIiwianNfc3JjXFxhcHBcXHBlcnNvbi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7O0FDQUM7Ozs7OztBQUVELElBQUksU0FBUyxxQkFBVyxLQUFYLEVBQWtCLFVBQWxCLENBQWI7O0FBRUE7O0FBRUEsRUFBRSxtQkFBRixFQUF1QixLQUF2QixDQUE2QixVQUFTLEtBQVQsRUFBZ0I7QUFDekM7QUFDQSxNQUFFLElBQUYsRUFBUSxRQUFSLENBQWlCLFFBQWpCLEVBQTJCLFFBQTNCLEdBQXNDLFdBQXRDLENBQWtELFFBQWxEO0FBQ0gsQ0FIRDs7Ozs7Ozs7Ozs7OztJQ05zQixNO0FBQ2xCLG9CQUFhLFNBQWIsRUFBd0IsUUFBeEIsRUFBa0M7QUFBQTs7QUFDOUIsYUFBSyxTQUFMLEdBQWlCLFNBQWpCO0FBQ0EsYUFBSyxRQUFMLEdBQWdCLFFBQWhCO0FBQ0g7Ozs7dUNBRWM7QUFDWCxtQkFBTyxLQUFLLFNBQVo7QUFDSDs7O3NDQUVhO0FBQ1YsbUJBQU8sS0FBSyxRQUFaO0FBQ0g7Ozs7OztrQkFaaUIsTSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCLvu79pbXBvcnQgUGVyc29uIGZyb20gXCIuL3BlcnNvblwiO1xyXG5cclxubGV0IHBlcnNvbiA9IG5ldyBQZXJzb24oXCJSYW1cIiwgXCJLdWxrYXJuaVwiKTtcclxuXHJcbi8vZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJuYW1lU3BhblwiKS5pbm5lckhUTUwgPSBwZXJzb24uZ2V0Rmlyc3ROYW1lKCkgKyBcIiBcIiArIHBlcnNvbi5nZXRMYXN0TmFtZSgpO1xyXG5cclxuJCgnI215dGFibGUgdGJvZHkgdHInKS5jbGljayhmdW5jdGlvbihldmVudCkge1xyXG4gICAgLy8kKHRoaXMpLmFkZENsYXNzKCdoaWdobGlnaHQnKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKCdoaWdobGlnaHQnKTtcclxuICAgICQodGhpcykuYWRkQ2xhc3MoJ2FjdGl2ZScpLnNpYmxpbmdzKCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG59KTsiLCLvu79leHBvcnQgZGVmYXVsdCBjbGFzcyBQZXJzb24ge1xyXG4gICAgY29uc3RydWN0b3IgKGZpcnN0TmFtZSwgbGFzdE5hbWUpIHtcclxuICAgICAgICB0aGlzLmZpcnN0TmFtZSA9IGZpcnN0TmFtZTtcclxuICAgICAgICB0aGlzLmxhc3ROYW1lID0gbGFzdE5hbWU7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGdldEZpcnN0TmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5maXJzdE5hbWU7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGdldExhc3ROYW1lKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmxhc3ROYW1lO1xyXG4gICAgfVxyXG59Il19

//# sourceMappingURL=app.js.map
